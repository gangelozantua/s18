let trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ["Pikachu","Charizard","Squirtle"],
	friends: {
		hoenn: ["May", "Max"],
		kanto: ["Brock", "Misty"]
	},
	talk: function(){
		console.log("Pikachu! I choose you!")
	}
}

console.log(trainer);

console.log(trainer.name);
console.log(trainer["age"]);
console.log(trainer.pokemon[1]);
console.log(trainer["friends"]["kanto"])

trainer.talk();


function pokemon(name,lvl,hp,atk){
	
	this.name = name,
	this.level = lvl,
	this.health = parseFloat((hp * 1.5).toFixed(0)),
	this.attack = parseFloat((lvl * 1.2).toFixed(2)),
	this.tackle = function(target){

		let currentHp = target.health - this.attack;

		console.log(`${this.name} tackled ${target.name}`)

		target.health = parseFloat(currentHp.toFixed(0))

		if (target.health > 0){
			console.log(`${target.name}'s health is now reduced to ${currentHp.toFixed(0)}`)
		}
		if (target.health <= 0){
			console.log(`${target.name}'s health is now reduced to 0`)
			target.faint(target)
		}
		return target
	},
	this.faint = function(target){	
		return console.log(`${target.name} fainted`)
	}	
}

let pikachu = new pokemon("Pikachu",12,24,12);
let geodude = new pokemon("Geodude",8,16,8);
let mewtwo = new pokemon("Mewtwo",100,200,100);

console.log(pikachu)
console.log(geodude.tackle(pikachu))
console.log(geodude.tackle(pikachu))
console.log(geodude.tackle(pikachu))
console.log(geodude.tackle(pikachu))


trainer.capture = function(target){
	
	console.log(`${this.name} captured ${target.name}`)
	this.pokemon.push(target.name);
	return this.pokemon
}

console.log(trainer.capture(mewtwo));
console.log(trainer.capture(geodude));

trainer.release = function(target){
	console.log(`${this.name} released ${target.name}`)
	this.pokemon.pop(target.name)
	return this.pokemon
}

console.log(trainer.release(mewtwo))


/*
let pikachu2 = new pokemon("Pikachu",12,24,12);
pikachu2.type = "Electric"
pikachu2.thunderbolt = function(target){

		let currentHp = target.health - this.attack;

		console.log(`${this.name} used Thunderbolt!`)

		if (target.type == "Rock"){
			currentHp = target.health - this.attack * .75
			console.log("It's not very effective!")
		}

		console.log(`${target.name}'s health is now reduced to ${currentHp.toFixed(0)}`)

		target.health = parseFloat(currentHp.toFixed(0))

		if (target.health <= 0){
			target.faint(target)
		}
}


let geodude2 = new pokemon("Geodude",8,16,8);
geodude2.type = "Rock"
geodude2.rockpunch = function(target){

		let currentHp = target.health - this.attack;

		console.log(`${this.name} used Rock Punch!`)

		if (target.type == "Electric"){
			currentHp = target.health - this.attack * 1.25
			console.log("It's very effective!")
		}

		console.log(`${target.name}'s health is now reduced to ${currentHp.toFixed(0)}`)

		target.health = parseFloat(currentHp.toFixed(0))

		if (target.health <= 0){
			target.faint(target)
		}
}

console.log(pikachu2.thunderbolt(geodude2));
console.log(geodude2.rockpunch(pikachu2));
console.log(pikachu2.thunderbolt(geodude2));
console.log(geodude2.rockpunch(pikachu2));
console.log(pikachu2.thunderbolt(geodude2));
*/