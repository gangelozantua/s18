console.log('Hello Javascript Objects');

//array - a collection of related data
const grades = [91,92,93,94,95];
/*const names = ['Joy', 'Natalia', 'Bianca']*/

//object - collection of multiple values with different data types
const objGrades = {
	//property: value (key-value pairs)
	firstName: "Aaron",
	lastName: "Delos Santos",
	firstGrading: 91,
	subject: 'English',
	teachers: ['Carlo', 'Masahiro'],
	isActive: true,
	schools: {
		city: 'Manila',
		country: 'Philippines'
	},
	studentNames: [
		{
			name: 'Adrian',
			batch: '152'
		},
		{
			name: 'Nikko',
			batch: '152'
		}
	],
//this - within the object
	description: function(){
		return `${this.subject}: ${this.firstGrading} of students ${this.studentNames[0].name} and ${this.studentNames[1].name}`
	}
}

//How do we access properties of an object?
	//dot notation (.)
	//bracket notation ([""])

//syntax:
/*
	objReference.propertyName - can use expressions
	objReference.["propertyName"] - cannot use expressions

*/

console.log(objGrades.firstGrading);//91
console.log(objGrades.subject);//English
console.log(objGrades["isActive"]);//true
console.log(objGrades["teachers"]);//carlo,masahiro
console.log(objGrades["firstName"]);//Aaron
console.log(objGrades["lastName"]);//Delos Santos

console.log(objGrades.description());//English: 91 of students Adrian and Nikko

console.log(objGrades.schools.country);//Philippines
console.log(objGrades["schools"]["city"]);//Manila

console.log(objGrades.studentNames[1]);//nikko, batch 152
console.log(objGrades["studentNames"][1]);//nikko, batch 152

console.log(objGrades.studentNames[1].name);//nikko
console.log(objGrades["studentNames"][1]["batch"]);//152
console.log(objGrades.studentNames[1]["batch"]);//152



//Is it possible to add a new property in an object? -yes
	//with the use of dot notation(.) and assignment operator (=)
	//you can also change the value of the property
objGrades.semester = "first";
/*console.log(objGrades);*/


//Is it possible to delete a property in an object? -yes
delete objGrades.semester;
console.log(objGrades);


//Mini Activity
/*
Given a set of objects:

-Compute for the average grade of each student object.
-Add the computed average of the student as a value to a new property called average to the studentGrades array of objects.
-Log the modified object array (includes the average property) to the console.

Stretch Goals:
Hint: Research the use of parseFloat() and toFixed()
-Round off the average into a single decimal number.

Note: Value for average property cannot be a string.

*/
const studentGrades = [
    { studentId: 1, Q1: 89.3, Q2: 91.2, Q3: 93.3, Q4: 89.8 },
    { studentId: 2, Q1: 69.2, Q2: 71.3, Q3: 76.5, Q4: 81.9 },
    { studentId: 3, Q1: 95.7, Q2: 91.4, Q3: 90.7, Q4: 85.6 },
    { studentId: 4, Q1: 86.9, Q2: 74.5, Q3: 83.3, Q4: 86.1 },
    { studentId: 5, Q1: 70.9, Q2: 73.8, Q3: 80.2, Q4: 81.8 }
];

/*
studentGrades[0].Average = (studentGrades[0].Q1+studentGrades[0].Q2+studentGrades[0].Q3+studentGrades[0].Q4) / 4;

studentGrades[1].Average = (studentGrades[1].Q1+studentGrades[1].Q2+studentGrades[1].Q3+studentGrades[1].Q4) / 4;

studentGrades[2].Average = (studentGrades[2].Q1+studentGrades[2].Q2+studentGrades[2].Q3+studentGrades[2].Q4) / 4;

studentGrades[3].Average = (studentGrades[3].Q1+studentGrades[3].Q2+studentGrades[3].Q3+studentGrades[3].Q4) / 4;

studentGrades[4].Average = (studentGrades[4].Q1+studentGrades[4].Q2+studentGrades[4].Q3+studentGrades[4].Q4) / 4;

console.log(studentGrades)
*/

/*
let ave1 = (studentGrades[0].Q1 + studentGrades[0].Q2 + studentGrades[0].Q3 + studentGrades[0].Q4) / 4
studentGrades[0].Average = parseFloat(ave1.toFixed(1));

let ave2 = (studentGrades[1].Q1 + studentGrades[1].Q2 + studentGrades[1].Q3 + studentGrades[1].Q4) / 4
studentGrades[1].Average = parseFloat(ave2.toFixed(1));

let ave3 = (studentGrades[2].Q1 + studentGrades[2].Q2 + studentGrades[2].Q3 + studentGrades[2].Q4) / 4
studentGrades[2].Average = parseFloat(ave3.toFixed(1));

let ave4 = (studentGrades[3].Q1 + studentGrades[3].Q2 + studentGrades[3].Q3 + studentGrades[3].Q4) / 4
studentGrades[3].Average = parseFloat(ave4.toFixed(1));

let ave5 = (studentGrades[4].Q1 + studentGrades[4].Q2 + studentGrades[4].Q3 + studentGrades[4].Q4) / 4
studentGrades[4].Average = parseFloat(ave5.toFixed(1));

console.log(studentGrades)
*/


/*
for(let i = 0; i < studentGrades.length; i++){
	let ave = (studentGrades[i].Q1 + studentGrades[i].Q2 + studentGrades[i].Q3 +studentGrades[i].Q4) / 4

	studentGrades[i].average = parseFloat(ave.toFixed(1))
}

console.log(studentGrades)
*/

studentGrades.forEach(function(element){
	let ave = (element.Q1 + element.Q2 + element.Q3 + element.Q4) / 4
	element.average = parseFloat(ave.toFixed(1))
})

console.log(studentGrades)



//Object Constructor
/*


*/


let myPokemon = {
	name: 'Pikachu',
	level: 3,
	health: 100,
	attack: 50,
	attack: function(){
		console.log(`This Pokemon tackled targetPokemon`)
		console.log(`targetPokemon's health is now reduced to targetPokemonhealth`)
	},
	faint:function(){
		console.log(`Pokemon fainted`)
	}
}
/*console.log(myPokemon)*/


function Pokemon(name,lvl,hp){

	
	this.name = name,
	this.level = lvl,
	this.health = hp * 2,
	this.attack = lvl,
	this.tackle = function(target){
		
		console.log(target)//object of charizard
		console.log(`${this.name} tackled ${target.name}`)
		console.log(`${target.name}'s health is now reduced to ${target.health - this.attack}`)
		
		//using compound assignment operator
		target.health -= this.attack

		if (target.health < 10) {		
			target.faint(target)
		}

	},
	this.faint = function(target){	
		console.log(`${target.name} fainted`)
	}	
}

let pikachu = new Pokemon("Pikachu",5,50);
let charizard = new Pokemon("Charizard",8,10);

console.log(pikachu.tackle(charizard))
console.log(pikachu.tackle(charizard))
console.log(pikachu.tackle(charizard))


/* Mini Activity

1. Create a new set of pokemon for battle (use Pokemon object constructor)
2. Solve the health of the pokemon that when tackled, the current value of target's health must reduced continuously as many times as the  tackle function is invoked.
3. If health of the target pokemon is now below 10, invoke the faint function. (Note: refactor faint function to display appropriate value to the statement)

*/


let squirtle = new Pokemon("Squirtle",5,20);
let bulbasaur = new Pokemon("Bulbasaur",8,20);

console.log(squirtle)
console.log(bulbasaur)

console.log(squirtle.tackle(bulbasaur))
console.log(squirtle.tackle(bulbasaur))





//REVIEW

	//Creating an object with object literals
		//{} - {objectLiteral}
		//[] - [arrayLiteral]

		let object = {
			property: "value",
			property2: "value2"
		}

		let array = ["sample1", "sample2"];

		console.log(typeof object);//object
		console.log(typeof array);//object

		//Arrays are a special type of data/object

		let object1 = {}

		console.log({} === object1);//false

		/*
			Any instance of an object is unique. No two objects are the same and thus we cannot use the ==/=== equality comparison on the objects themselves, however we can compare their properties.
		*/

		let object2 = {
			name: "sample"
		}
		let object3 = {
			name: "sample"
		}

		console.log(object2 == object3);//false
		console.log(object2.name == object3.name);//true

	//Creating an object with constructor

	function Dog(name,breed,age){
		
		//this. keyword refers to the object you will create with your constructor
		this.name = name,
		this.breed = breed,
		this.age = age * 7,
		//we can also add methods to constructor
		//methods are functions inside object.
		this.bark = function(){
			console.log("Woof!")
		}
	}

	//new keyword to create a new object our of your constructor
	//if you don't use the new keyword, the object will not be created.

	let dog1 = new Dog('Bolt', 'Corgi', 5);
	console.log(dog1)

	/*
		Conventions when creating our constructor:
			To distinguish it with other functions we usually capitalize the name of our constructor.
	*/

